# Deploy an example Drupal site.
# TODO Move this to a definition with parameters.
require_recipe "mysql"
#require_recipe "drush"
require_recipe "drush_make"

docroot = "#{node[:www_root]}/drupal/www"

cookbook_file "#{node[:www_root]}/drupal/drupal.make" do
  source node[:drupal][:makefile]
  #notifies :restart, resources("service[apache2]"), :delayed
end

# Download and install a fresh drupal.
# We only do this if there's not alreayd a Drupal installation
# here.
bash "run-drush-make" do
  code "drush make #{node[:www_root]}/drupal/drupal.make #{node[:www_root]}/drupal/fresh"
  not_if { File.exists? "#{docroot}/index.php" }
end

# Since Drush insists upon creating its own directory, yet we need to
# create the directories ahead of time for Apache, we have to perform
# monumental acts of stupidity like this.
bash "relocate-dmake-files" do
  code "mv #{node[:www_root]}/drupal/fresh/* #{node[:www_root]}/drupal/fresh/.??* #{docroot}"
  #code "mv #{node[:www_root]}/drupal/fresh/* #{docroot} && mv #{node[:www_root]}/drupal/fresh/.* #{docroot}"
  not_if { File.exists? "#{docroot}/index.php" }
end
bash "kill-old-dmake" do
  code "rm -rf #{node[:www_root]}/drupal/fresh"
end

# For each site:
# - create a site-specific database.
# - create a site-specific directory.
# - create a default settings file.
node[:hosts][:localhost_aliases].each do |a|
  db_name = a.gsub('.','_')
  execute "create-#{db_name}" do
    command "/usr/bin/mysql -u root -p#{node[:mysql][:server_root_password]} -e \"" +
      "CREATE DATABASE IF NOT EXISTS #{db_name};\""
    action :run
    ignore_failure true
  end

  # Create the site directory.
  directory "#{docroot}/sites/#{a}" do
    action :create
    recursive true
  end

  # Create the files directory and set perms to 777.
  # This is outside of the docroot because of issues
  # setting directories on the host OS to 0777.
  # directory "#{docroot}/sites/#{a}/files" do
  directory "/var/www/#{a}/files" do
    action :create
    owner 'www-data'
    mode 0777
    recursive true
  end

  # Create a symbolic link to the file.
  #link "#{docroot}/sites/#{a}/files" do
  #  to "/var/www/#{a}/files"
  #end

  # cookbook_file "#{docroot}/sites/#{a}/settings.php" do
  #   mode 0777
  #   source "settings.php"
  # end

  template "#{docroot}/sites/#{a}/settings.php" do
    source "settings-multisite.php.erb"
    mode 0777
    #not_if { File.exists? "#{docroot}/sites/#{a}/settings.php" }
    variables(:db_name => db_name, :db_user => 'root', :db_pass => node[:mysql][:server_root_password])
  end
end
