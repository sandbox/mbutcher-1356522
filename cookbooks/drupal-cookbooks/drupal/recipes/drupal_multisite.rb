# Automatically prepare vhosts for drupal sites.
# TODO Make this configurable per host.
require_recipe "hosts"
require_recipe "apache2"

# Make sure that the document root exists
bash "create-document-root" do
  code "mkdir -p #{node[:www_root]}/drupal/www"
  action :run
  ignore_failure true
end

# Configure the development site
web_app node[:hosts][:localhost_aliases][0] do
  template "site-multisite.conf.erb"
  server_port 80
  server_name node[:hosts][:localhost_aliases][0]
  server_aliases node[:hosts][:localhost_aliases]
  docroot "#{node[:www_root]}/drupal/www"
end
