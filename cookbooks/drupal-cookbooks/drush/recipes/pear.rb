# Install Drush from PEAR.
# Author: Matt Butcher (Technosophos)

php_pear "Console_Table" do
  action :install
end
# php_pear_channel "pear.drush.org" do
#   action :discover
#   #channel_name "pear.drush.org"
#   channel_xml "channel.xml"
# end
php_pear "pear.drush.org/drush" do
  action :install
end



  # bash "install-drush" do
  #   code <<-EOH
  #   (pear channel-discover pear.drush.org && pear install drush/drush)
  #   EOH
  # end
  # bash "install-console-table" do
  #   code <<-EOH
  #   (pear install Console_Table)
  #   EOH
  #   not_if "pear list| grep Console_Table"
  # end