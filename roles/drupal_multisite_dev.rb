# This file sets up a Drupal multi-site.
# Varnish is turned OFF for this setup.
name "drupal_multisite_dev"
description "A LAMP stack for Drupal designed for multi-site configurations."
run_list(
  "role[apache2_mod_php]",
  # "role[apache2_backend]", # Overrides ports.
  "role[drupal]",
  "role[drupal_dev]",
  # "role[memcached]", # sets up memcache
  "role[mysql_server]",
  # "role[varnish_frontend]", # 
  "recipe[drupal::drupal_multisite]"
)
